from django.urls import path
from api.views import LinkView, ModelView


urlpatterns = [
    path('links/', LinkView.as_view()),
    path('articles/', ModelView.as_view()),
    path('posts/', ModelView.as_view()),
    path('videos/', ModelView.as_view()),
    path('products/', ModelView.as_view()),
]
