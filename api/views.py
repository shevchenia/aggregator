from rest_framework.generics import ListAPIView
from django.http import HttpResponseNotFound
from crawler.models import Link
from crawler.serializers import LinksSerializer
from aggregator.constants import SITE_NAMES
from aggregator.factory import Factory


class LinkView(ListAPIView):
    queryset = Link.objects.all()
    serializer_class = LinksSerializer

    def get(self, request, *args, **kwargs):
        request_data = request.parser_context.get('request').query_params
        site = SITE_NAMES.get(request_data.get('site'))
        type_data = request_data.get('type')
        if not site:
            return HttpResponseNotFound('site not found')
        objects = self.queryset.filter(site=site)
        if type_data:
            objects = objects.filter(type=type_data)
        page = self.paginate_queryset(objects)
        serializer = self.get_serializer(page, many=True)
        return self.get_paginated_response(serializer.data)


class ModelView(ListAPIView):

    def get(self, request, *args, **kwargs):
        request_data = request.parser_context.get('request').query_params
        site_name = request_data.get('site')
        site_url = SITE_NAMES.get(site_name)
        type_data = request_data.get('type')
        if not site_url:
            return HttpResponseNotFound('site not found')
        self.queryset = Factory().get_model(site_name).objects.filter(link__site=site_url)
        self.serializer_class = Factory().get_serializer(site_name)
        if type_data:
            self.queryset = self.queryset.filter(link__type=type_data)
        page = self.paginate_queryset(self.queryset)
        serializer = self.get_serializer(page, many=True)
        return self.get_paginated_response(serializer.data)
