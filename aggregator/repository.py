from crawler.models import Link
from scraper.models import Article, Product, Video, Post
from abc import ABC, abstractmethod


class Repository(ABC):

    @abstractmethod
    def save(self, links: list):
        pass


class LinkRepository(Repository):

    def save(self, links: list):
        objects_to_save = []
        for link in links[0]:
            if not Link.objects.filter(link=link).exists():
                objects_to_save.append(Link(site=links[2], link=link, type=links[1]))
        Link.objects.bulk_create(objects_to_save)
        print(f'Сохранено {len(objects_to_save)} ссылок в базу данных')


class ArticleRepository(Repository):

    def save(self, links: list):
        objects_to_save = []
        for article in links:
            if not Article.objects.filter(link=article[0]).exists():
                objects_to_save.append(Article(link=article[0], title=article[1], create_date=article[2],
                                               text=article[3]))
        Article.objects.bulk_create(objects_to_save)
        print(f'Сохранено {len(objects_to_save)} статей в базу данных')


class VideoRepository(Repository):

    def save(self, links: list):
        objects_to_save = []
        for video_clip in links:
            if not Video.objects.filter(link=video_clip[0]).exists():
                objects_to_save.append(Video(link=video_clip[0], name=video_clip[1], views=video_clip[2],
                                             likes=video_clip[3], dislikes=video_clip[4], data=video_clip[5]))
        Video.objects.bulk_create(objects_to_save)
        print(f'Сохранено {len(objects_to_save)} ссылок на видеоролики в базу данных')


class ProductRepository(Repository):

    def save(self, links: list):
        objects_to_save = []
        for product in links:
            if not Product.objects.filter(link=product[0]).exists():
                objects_to_save.append(Product(link=product[0], title=product[1], price=product[2],
                                               type=product[3], category=product[4]))
        Product.objects.bulk_create(objects_to_save)
        print(f'Сохранено {len(objects_to_save)} товаров в базу данных')


class PostRepository(Repository):

    def save(self, links: list):
        objects_to_save = []
        for post in links:
            if not Post.objects.filter(link=post[0]).exists():
                objects_to_save.append(Post(link=post[0], likes=post[1], reposts=post[2], views=post[3],
                                            posts=post[4]))
        Post.objects.bulk_create(objects_to_save)
        print(f'Сохранено {len(objects_to_save)} товаров в базу данных')
