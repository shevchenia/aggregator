from bs4 import BeautifulSoup
import time
import requests
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from aggregator.constants import SCROLL_PAUSE_TIME, HEADERS, MAX_SCROLL


class PageLoader:

    def get_page_selenium(self, url: str) -> BeautifulSoup:
        print('selenium: ', url)
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument("--window-size=1920,1080")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        chrome_options.add_argument('--disable-popup-blocking')

        driver = webdriver.Chrome(
            "/home/INTEXSOFT/dmitry.shevchenya/PycharmProjects/PythonProjectNews/aggregator/chromedriver",
            chrome_options=chrome_options)
        driver.get(url)
        time.sleep(2)
        last_height = driver.execute_script("return document.body.scrollHeight")
        count = 0
        while True:
            count += 1
            if count > 5:
                print(count)
            start = time.time()
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            time.sleep(SCROLL_PAUSE_TIME)
            new_height = driver.execute_script("return document.body.scrollHeight")
            if new_height == last_height or time.time() - start > 30 or count > MAX_SCROLL:
                break
            last_height = new_height
        response = driver.page_source
        main_page = BeautifulSoup(response, 'lxml')
        driver.close()
        return main_page

    def get_page_requests(self, url: str) -> BeautifulSoup:
        print('requests: ', url)
        response = requests.get(url, headers=HEADERS)
        main_page = BeautifulSoup(response.text, 'lxml')
        time.sleep(1)
        return main_page
