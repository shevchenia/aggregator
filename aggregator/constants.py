SCROLL_PAUSE_TIME = 2

MAX_SCROLL = 20

COUNT_PARSING_ZALANDO = {
    'pagination': 2,
    'outfits': 2,
}

COUNT_PARSING_SCRAPER = 5

PAGINATOR_OBJECTS = 7

HEADERS = {
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36'
}

SITE_NAMES = {
    'cnn': 'https://www.cnn.com/',
    'bbc': 'https://www.bbc.com/',
    'habr': 'https://habr.com/',
    'zalando': 'https://en.zalando.de/',
    'youtube': 'https://www.youtube.com/',
    'vk': 'https://vk.com/',
    }

CATEGORY_URLS_BBC = {
    'news': 'https://www.bbc.com/news',
    'sport': 'https://www.bbc.com/sport',
}

CATEGORY_URLS_HABR = {
    'news': 'https://habr.com/ru/news/page',
    'article': 'https://habr.com/ru/all/page',
}

CATEGORY_URLS_ZALANDO = {
    'women': 'https://en.zalando.de/women-home/',
    'men': 'https://en.zalando.de/men-home/',
    'kids': 'https://en.zalando.de/kids-home/',
}

YOUTUBE_CHANNELS = [
    'https://www.youtube.com/channel/UC9MK8SybZcrHR3CUV4NMy2g',
    'https://www.youtube.com/channel/UCQdPrDypfQeY5euAPbdc11g'
]

VK_PERSON = [
    'https://vk.com/markus_persson',
    'https://vk.com/cat_programming',
]
