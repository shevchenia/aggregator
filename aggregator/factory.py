from crawler.news_crawlers.crawler_cnn import CnnCrawler
from crawler.news_crawlers.crawler_bbc import BbcCrawler
from crawler.news_crawlers.crawler_habr import HabrCrawler
from crawler.shop_crawlers.crawler_zalando import ZalandoCrawler
from crawler.video_crawlers.crawler_youtube import YoutubeCrawler
from crawler.sotial_network_crawlers.crawler_vk import VkCrawler
from scraper.news_scrapers.scraper_cnn import CnnScraper
from scraper.news_scrapers.scraper_bbc import BbcScraper
from scraper.news_scrapers.scraper_habr import HabrScraper
from scraper.shop_scrapers.scraper_zalando import ZalandoScraper
from scraper.video_scrapers.scraper_youtube import YoutubeScraper
from scraper.sotial_network_scraper.scraper_vk import VkScraper
from scraper.models import Article, Product, Video, Post
from scraper.serializers import ArticleSerializer, ProductSerializer, VideoSerializer, PostSerializer
from aggregator.repository import ArticleRepository, ProductRepository, VideoRepository, PostRepository


class Factory:

    def __init__(self):
        self.objects = {
            'cnn': {'crawler': CnnCrawler, 'scraper': CnnScraper, 'model': Article, 'serializer': ArticleSerializer,
                    'repository': ArticleRepository, 'name': 'article'},
            'bbc': {'crawler': BbcCrawler, 'scraper': BbcScraper, 'model': Article, 'serializer': ArticleSerializer,
                    'repository': ArticleRepository, 'name': 'article'},
            'habr': {'crawler': HabrCrawler, 'scraper': HabrScraper, 'model': Article, 'serializer': ArticleSerializer,
                     'repository': ArticleRepository, 'name': 'article'},
            'zalando': {'crawler': ZalandoCrawler, 'scraper': ZalandoScraper, 'model': Product,
                        'serializer': ProductSerializer, 'repository': ProductRepository, 'name': 'product'},
            'youtube': {'crawler': YoutubeCrawler, 'scraper': YoutubeScraper, 'model': Video,
                        'serializer': VideoSerializer, 'repository': VideoRepository, 'name': 'video'},
            'vk': {'crawler': VkCrawler, 'scraper': VkScraper, 'model': Post, 'serializer': PostSerializer,
                   'repository': PostRepository, 'name': 'Post'},
        }

    def get_crawler(self, site_name):
        object = self.objects.get(site_name)
        if object:
            return object.get('crawler')()
        raise Exception("Not Found")

    def get_scraper(self, site_name):
        object = self.objects.get(site_name)
        if object:
            return object.get('scraper')()
        raise Exception("Not Found")

    def get_model(self, site_name):
        object = self.objects.get(site_name)
        if object:
            return object.get('model')
        raise Exception("Not Found")

    def get_serializer(self, site_name):
        object = self.objects.get(site_name)
        if object:
            return object.get('serializer')
        raise Exception("Not Found")

    def get_repository(self, site_name):
        object = self.objects.get(site_name)
        if object:
            return object.get('repository')()
        raise Exception("Not Found")

    def get_name_model(self, site_name):
        object = self.objects.get(site_name)
        if object:
            return object.get('name')
        raise Exception("Not Found")
