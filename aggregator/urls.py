from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('api.urls')),
    path('api/crawler/', include('crawler.urls')),
    path('api/scraper/', include('scraper.urls')),
    path('', include('user_interface.urls')),
]
