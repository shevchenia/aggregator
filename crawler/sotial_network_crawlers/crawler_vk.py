from bs4 import BeautifulSoup
from crawler.crawler import Crawler
from aggregator.constants import SITE_NAMES, VK_PERSON
from aggregator.page_loader import PageLoader


class VkCrawler(Crawler):
    URL = SITE_NAMES.get('vk')

    def crawl(self, type: str) -> (list, str):
        if type:
            raise Exception("VkCrawler does not support type.")
        post_urls = []
        for account in VK_PERSON:
            post_urls.extend(self.get_info(PageLoader().get_page_selenium(account), account))
        return post_urls, 'post'

    def get_info(self, page: BeautifulSoup, url: str) -> list:
        name = page.find('h1').get_text()
        blocks = page.find_all('div', class_='_post_content')
        count = 0
        text_data = f'{name}, {url}\n'
        links = []
        for block in blocks:
            count += 1
            link = block.find("a", class_="post_link")
            if not link:
                continue
            link = f'https://vk.com/{link.get("href")}'
            links.append(link)
            text = block.find('div', class_='wall_post_text')
            data = block.find('span', class_='rel_date')
            data = data.get_text().replace("\xa0", " ")
            text_data += f'{count}. {link}, {data}, '
            photo = block.find('div', class_='wall_text').find("a", {"aria-label": "photo"})
            if photo and photo.get("href"):
                text_data += f'link photo: https://vk.com/{photo.get("href")}, '
            else:
                photo = block.find('div', class_='wall_text').find("a", {"aria-label": "фотография"})
                if photo and photo.get("href"):
                    text_data += f'link photo: https://vk.com/{photo.get("href")}, '
            if text:
                text_data += f'text: {text.get_text()}'
            if not photo and not text:
                text_data += 'Error (not found data)'
            text_data += '\n'
        with open(f'data_{name}.txt', 'w', encoding='utf-8') as file:
            file.write(text_data)
            print(f'file data_{name}.txt save')
        return links
