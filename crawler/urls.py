from django.urls import path
from .views import CrawlerView


urlpatterns = [
    path('<str:site_name>/', CrawlerView.as_view()),
]
