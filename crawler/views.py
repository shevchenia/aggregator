from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from django.http import HttpResponseNotFound
from crawler.models import Link
from aggregator.factory import Factory
from aggregator.repository import LinkRepository
from crawler.serializers import LinksSerializer
from aggregator.constants import SITE_NAMES


class CrawlerView(ListAPIView):
    queryset = Link.objects.all().order_by('id')
    serializer_class = LinksSerializer

    def get(self, request, *args, **kwargs):
        site_name = request.parser_context['kwargs']['site_name']
        site_url = SITE_NAMES.get(site_name)
        instance = Factory().get_crawler(site_name)
        try:
            links, type = instance.crawl(request.query_params.get('type'))
        except Exception as Error:
            print(Error)
            return HttpResponseNotFound('Данная страница не найдена')
        LinkRepository().save([links, type, site_url])
        objects = self.queryset.filter(site=site_url)
        page = self.paginate_queryset(objects)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        return Response(LinksSerializer(page, many=True).data)
