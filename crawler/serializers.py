from rest_framework import serializers
from crawler.models import Link


class LinksSerializer(serializers.ModelSerializer):
    class Meta:
        model = Link
        fields = ('id', 'site', 'link', 'type')
