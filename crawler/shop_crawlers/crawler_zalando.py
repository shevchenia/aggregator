from bs4 import BeautifulSoup
from crawler.crawler import Crawler
from aggregator.constants import SITE_NAMES, CATEGORY_URLS_ZALANDO, COUNT_PARSING_ZALANDO
from aggregator.page_loader import PageLoader


class ZalandoCrawler(Crawler):
    URL = SITE_NAMES.get('zalando')

    def crawl(self, type: str) -> (list, str):
        url = CATEGORY_URLS_ZALANDO.get(type)
        if not url:
            raise Exception("ZalandoCrawler does not support type.")
        category_urls = self.get_categories(PageLoader().get_page_selenium(url))
        pages_urls = []
        for category_url in category_urls:
            pages_urls.extend(self.get_pages(PageLoader().get_page_selenium(category_url), category_url))
        product_links = []
        for pages_url in pages_urls:
            product_links.extend(self.get_links(PageLoader().get_page_selenium(pages_url)))
        return product_links, 'Product'

    def get_categories(self, page: BeautifulSoup) -> list:
        links = []
        for link in page.find('nav', class_="z-navicat-header_categoryContainer").find_all('a'):
            free_link = link.get('href')
            if 'http' not in free_link:
                free_link = self.URL[:-1] + free_link
            links.append(free_link)
        return links

    def get_pages(self, page: BeautifulSoup, url: str) -> list:
        if page.find('nav', class_="adFHlH _0xLoFW _7ckuOK mROyo1 uEg2FS"):
            return [f'{url}?p={number + 1}' for number in range(COUNT_PARSING_ZALANDO.get('pagination'))]
        return [url]

    def get_links(self, page: BeautifulSoup) -> list:
        links, outfits = [], []
        for article in page.find_all('article'):
            for link in article.find_all('a'):
                free_link = str(link.get('href'))
                if 'outfits' in free_link:
                    outfits.append(self.URL[:-1] + free_link)
                elif 'html' in free_link:
                    links.append(free_link)
        count = COUNT_PARSING_ZALANDO.get('outfits')
        if len(outfits) > count:
            outfits = outfits[:count]
        for outfit in outfits:
            page = PageLoader().get_page_selenium(outfit)
            for article in page.find('div', class_="escape-grid__highlight-items").find_all('article'):
                for link in article.find_all('a'):
                    links.append(str(link.get('href')))
        return links
