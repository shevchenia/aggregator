from django.db import models


class Link(models.Model):
    site = models.CharField(max_length=255)
    link = models.TextField()
    type = models.CharField(max_length=20)

    class Meta:
        ordering = ['id']

    def __str__(self):
        return self.link
