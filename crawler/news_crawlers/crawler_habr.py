from bs4 import BeautifulSoup
from crawler.crawler import Crawler
from aggregator.constants import SITE_NAMES, CATEGORY_URLS_HABR
from aggregator.page_loader import PageLoader


class HabrCrawler(Crawler):
    URL = SITE_NAMES.get('habr')

    def crawl(self, type: str) -> (list, str):
        url = CATEGORY_URLS_HABR.get(type)
        links = []
        if not url:
            raise Exception("HabrCrawler does not support type.")
        for number in range(1, 51):
            links.extend(self.get_links(PageLoader().get_page_requests(f'{url}{str(number)}/')))
        return links, type

    def get_links(self, page: BeautifulSoup) -> list:
        links = []
        for article in page.find_all('article'):
            link = article.find_all('a')
            links.append(self.URL[:-1] + link[2].get('href'))
        return links
