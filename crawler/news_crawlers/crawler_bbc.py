import re
from bs4 import BeautifulSoup
from crawler.crawler import Crawler
from aggregator.constants import CATEGORY_URLS_BBC, SITE_NAMES
from aggregator.page_loader import PageLoader


class BbcCrawler(Crawler):
    URL = SITE_NAMES.get('bbc')

    def crawl(self, type: str) -> (list, str):
        url = CATEGORY_URLS_BBC.get(type)
        if not url:
            raise Exception("BbcCrawler does not support type.")
        subcategory_urls = self._get_categories(PageLoader().get_page_requests(url), str(url).split('/')[-1])
        article_urls = []
        for subcategory_url in subcategory_urls:
            article_urls.extend(self._get_article_links(PageLoader().get_page_requests(subcategory_url)))
        return article_urls, type

    def _get_categories(self, page: BeautifulSoup, category_name: str) -> list:
        subcategory_urls = []
        for navigation in page.find_all('nav'):
            for link in navigation.find_all('a'):
                free_link = str(link.get('href'))
                if category_name in free_link and 'http' not in free_link:
                    subcategory_urls.append(f'https://www.bbc.co.uk{free_link}')
        return subcategory_urls

    def _get_article_links(self, page: BeautifulSoup) -> list:
        links_article = []
        for link in page.find_all('a'):
            free_link = str(link.get('href'))
            if re.findall(r'\d{8}', free_link) and free_link[0] == '/':
                if 'http' not in free_link:
                    free_link = f'https://www.bbc.co.uk{free_link}'
                links_article.append(free_link)
        return links_article

