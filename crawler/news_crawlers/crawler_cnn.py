from bs4 import BeautifulSoup, Tag
from crawler.crawler import Crawler
from aggregator.constants import SITE_NAMES
from aggregator.page_loader import PageLoader


class CnnCrawler(Crawler):
    URL = SITE_NAMES.get('cnn')

    def crawl(self, type: str) -> (list, str):
        if type is not None:
            raise Exception("CnnCrawler does not support type.")
        category_urls = self._get_categories(PageLoader().get_page_requests(self.URL))
        subcategory_urls = []
        for category_url in category_urls:
            subcategory_urls.extend(self._get_subcategories(PageLoader().get_page_requests(category_url)))
        subcategory_urls = list(set(subcategory_urls))
        article_urls = []
        for subcategory_url in subcategory_urls:
            article_urls.extend(self._get_article_links(PageLoader().get_page_requests(subcategory_url)))
        return article_urls, 'news'

    def _get_categories(self, page: BeautifulSoup) -> list:
        links = page.find('header').find('nav').find_all('a')
        return ['https://www.cnn.com' + str(link.get('href')) for link in links]

    def _get_subcategories(self, page: BeautifulSoup) -> list:
        subcategories_urls = []
        for navigation in [navigation for navigation in page.find('nav') if isinstance(navigation, Tag)]:
            for link in [link for link in navigation.find_all('a') if isinstance(navigation, Tag)]:
                free_link = str(link.get('href'))
                if 'http' not in free_link and len(free_link) < 230:
                    subcategories_urls.append(f'https://www.cnn.com{free_link}')
                elif 'cnn.com' in free_link:
                    subcategories_urls.append(free_link)
        return subcategories_urls

    def _get_article_links(self, page: BeautifulSoup) -> list:
        article_urls = []
        for link in page.find_all('a'):
            link_free = str(link.get('href'))
            if 'index.html' in link_free:
                article_urls.append(f'https://www.cnn.com{link_free}')
        return article_urls
