from bs4 import BeautifulSoup
from crawler.crawler import Crawler
from aggregator.constants import YOUTUBE_CHANNELS, SITE_NAMES
from aggregator.page_loader import PageLoader


class YoutubeCrawler(Crawler):
    URL = SITE_NAMES.get('youtube')

    def crawl(self, type: str) -> (list, str):
        if type:
            raise Exception("YotubeCrawler does not support type.")
        links = []
        playlist_urls = []
        for channel_url in YOUTUBE_CHANNELS:
            playlist_urls.extend(self.get_playlist(PageLoader().get_page_selenium(f'{channel_url}/playlists?view=1')))
        for playlist_url in playlist_urls:
            links.extend(self.get_links(PageLoader().get_page_selenium(playlist_url)))
        return links, 'Video'

    def get_playlist(self, page: BeautifulSoup) -> list:
        playlist_urls = []
        for content in page.find_all('a', id='thumbnail'):
            link = content.get('href')
            if link:
                playlist_urls.append(f'{self.URL}{link}')
        return playlist_urls

    def get_links(self, page: BeautifulSoup) -> list:
        links_video = []
        for link in page.find_all('a', id='wc-endpoint'):
            free_link = link.get('href')
            if free_link:
                links_video.append(f'{self.URL[:-1]}{free_link}')
        return links_video
