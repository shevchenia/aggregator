from django.shortcuts import render
from rest_framework.views import View
from django.core.paginator import Paginator
from crawler.models import Link
from aggregator.constants import SITE_NAMES, PAGINATOR_OBJECTS
from aggregator.factory import Factory


class LinkView(View):
    queryset = Link.objects.all()
    model = Link

    def get(self, request, *args, **kwargs):
        site_name = request.GET.get('menu1')
        if not site_name:
            return render(request, 'user_interface/main.html', context={})
        page = request.GET.get('page')
        if not page:
            page = 1
        site_url = SITE_NAMES.get(site_name)
        category = request.GET.get('menu2')
        obj = request.GET.get('menu3')
        objects = self.queryset.filter(site=site_url).filter(type=category) if obj == 'link' else \
            Factory().get_model(site_name).objects.filter(link__site=site_url).filter(link__type=category)
        model = 'link' if obj == 'link' else Factory().get_name_model(site_name)
        paginator = Paginator(objects, PAGINATOR_OBJECTS)
        page_number = request.GET.get('page')
        page_obj = paginator.get_page(page_number)
        context = {
            'page_obj': page_obj,
            'site': site_name,
            'category': category,
            'obj': obj,
            'page_number': page,
            'model': model,
        }
        return render(request, 'user_interface/main.html', context=context)


# class ArticleView(View):
#     queryset = Link.objects.all()
#     model = Link
#
#     def get(self, request, *args, **kwargs):
#         site_name = request.GET.get('menu1')
#         if not site_name:
#             return render(request, 'user_interface/main.html', context={})
#         page = request.GET.get('page')
#         if not page:
#             page = 1
#         site_url = SITE_NAMES.get(site_name)
#         category = request.GET.get('menu2')
#         obj = request.GET.get('menu3')
#         objects = self.queryset.filter(site=site_url).filter(type=category) if obj == 'link' else \
#             Factory().get_model(site_name).objects.filter(link__site=site_url).filter(link__type=category)
#         model = 'link' if obj == 'link' else Factory().get_name_model(site_name)
#         paginator = Paginator(objects, PAGINATOR_OBJECTS)
#         page_number = request.GET.get('page')
#         page_obj = paginator.get_page(page_number)
#         context = {
#             'page_obj': page_obj,
#             'site': site_name,
#             'category': category,
#             'obj': obj,
#             'page_number': page,
#             'model': model,
#         }
#         return render(request, 'user_interface/main.html', context=context)
