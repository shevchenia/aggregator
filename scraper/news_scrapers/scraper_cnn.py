import re
from scraper.scraper import Scraper
from crawler.models import Link
from aggregator.constants import SITE_NAMES
from aggregator.page_loader import PageLoader


class CnnScraper(Scraper):
    URL = SITE_NAMES.get('cnn')

    def scrape(self, link: Link) -> tuple:
        try:
            page = PageLoader().get_page_selenium(link.link)
            title = page.find('h1')
            create_date = re.findall(r'\d\d\d\d/\d\d/\d\d', str(link))
            article = page.find_all('article')
            if title and create_date and article:
                title = title.get_text()
                create_date = create_date[0].replace('/', '-')
                text = article[0].get_text().replace('\n', '').replace('\\', '')
                return link, title, create_date, text
        except Exception as error:
            print('Error', error)
        return link, 'None', '1900-01-01', 'None'
