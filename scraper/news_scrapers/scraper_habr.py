from scraper.scraper import Scraper
from crawler.models import Link
from aggregator.constants import SITE_NAMES
from aggregator.page_loader import PageLoader


class HabrScraper(Scraper):
    URL = SITE_NAMES.get('habr')

    def scrape(self, link: Link) -> tuple:
        try:
            page = PageLoader().get_page_requests(link.link)
            title = page.find('h1')
            create_date = page.find('time')
            article = page.find('article')
            text = ''
            if title and create_date and article:
                title = title.get_text()
                create_date = create_date.get('title')[:10]
                for block in article.find_all('div', id='post-content-body'):
                    text = text + block.get_text()
                return link, title, create_date, text
        except Exception as error:
            print('Error', error)
        return link, 'None', '1900-01-01', 'None'
