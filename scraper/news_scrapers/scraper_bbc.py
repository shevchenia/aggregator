from scraper.scraper import Scraper
from crawler.models import Link
from aggregator.constants import SITE_NAMES
from aggregator.page_loader import PageLoader


class BbcScraper(Scraper):
    URL = SITE_NAMES.get('bbc')

    def scrape(self, link: Link) -> tuple:
        try:
            page = PageLoader().get_page_requests(link.link)
            title = page.find('h1')
            create_date = page.find('time')
            article = page.find('article')
            text = ''
            if title and create_date and article:
                title = title.get_text()
                create_date = str(create_date.get('datetime'))[:10]
                blocks = article.find_all('div', {'data-component': "text-block"})
                if blocks:
                    for block in blocks:
                        text = text + block.get_text()
                if text == '':
                    blocks = article.find_all('p')
                    for block in blocks:
                        text = text + block.get_text()
                if text != '' and len(create_date) == 10:
                    return link, title, create_date, text
        except Exception as error:
            print('Error', error)
        return link, 'None', '1900-01-01', 'None'
