from rest_framework import serializers
from scraper.models import Article, Product, Video, Post


class ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = ('id', 'link', 'title', 'create_date', 'text')


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'link', 'title', 'price', 'type', 'category')


class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Video
        fields = ('link', 'name', 'views', 'likes', 'dislikes', 'data')


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ('link', 'likes', 'reposts', 'views', 'posts')
