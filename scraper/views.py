from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from django.http import HttpResponseNotFound
from crawler.models import Link
from aggregator.factory import Factory
from aggregator.constants import SITE_NAMES, COUNT_PARSING_SCRAPER


class ScraperView(ListAPIView):
    queryset = []
    serializer_class = ''

    def get(self, request, *args, **kwargs):
        site_name = request.parser_context['kwargs']['site_name']
        try:
            instance = Factory().get_scraper(site_name)
        except Exception as error:
            print('Error:', error)
            return HttpResponseNotFound('scraper not exists')
        self.queryset = Factory().get_model(site_name).objects.filter(link__site=instance.URL)
        self.serializer_class = Factory().get_serializer(site_name)
        links = Link.objects.filter(site=instance.URL)
        links = [link for link in links if not self.queryset.filter(link=link)]
        try:
            objects = instance.scrape_bulk(links[:COUNT_PARSING_SCRAPER])
        except Exception as error:
            print('Error:', error)
            return HttpResponseNotFound('Error scraper')
        Factory().get_repository(site_name).save(objects)
        print('Осталось обработать: ', len(links) - COUNT_PARSING_SCRAPER if len(links) > COUNT_PARSING_SCRAPER else 0)
        page = self.paginate_queryset(self.queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        return Response(Factory().get_serializer(site_name)(page, many=True).data)
