from django.urls import path
from scraper.views import ScraperView


urlpatterns = [
    path('<str:site_name>/', ScraperView.as_view()),
]
