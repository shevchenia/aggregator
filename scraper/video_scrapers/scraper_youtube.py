import re
from datetime import datetime
from scraper.scraper import Scraper
from crawler.models import Link
from aggregator.constants import SITE_NAMES
from aggregator.page_loader import PageLoader


class YoutubeScraper(Scraper):
    URL = SITE_NAMES.get('youtube')

    def scrape(self, link: Link) -> tuple:
        try:
            info = PageLoader().get_page_selenium(link.link).find('div', id='info-contents')
            name = info.find('h1').get_text()
            views = int(info.find('div', id='info-text').get_text().split()[0].replace(',', ''))
            likes = int(re.findall(r'\d+', info.find_all('button', id='button')[0].get('aria-label').replace(',', ''))[0])
            dislikes = int(re.findall(r'\d+', info.find_all('button', id='button')[1].get('aria-label').replace(',', ''))[0])
            data = datetime.strptime(info.find('div', id='info-strings').get_text().replace(',', ''), "%b %d %Y").strftime('%Y-%m-%d')
            return link, name, views, likes, dislikes, data
        except Exception as error:
            print('Error:', error)
        return link, 'None', 0, 0, 0, '1900-01-01'
