from scraper.scraper import Scraper
from crawler.models import Link
from aggregator.constants import SITE_NAMES
from aggregator.page_loader import PageLoader


class VkScraper(Scraper):
    URL = SITE_NAMES.get('vk')

    def scrape(self, link: Link) -> tuple:
        try:
            page = PageLoader().get_page_selenium(link.link)
            menu = page.find('div', class_='like_cont')
            likes = int(menu.find_all('a')[0].get('data-count'))
            reposts = int(menu.find_all('a')[1].get('data-count'))
            try:
                views = int(float(menu.find('div', class_='like_views').get_text().replace('K', ''))*1000)
            except Exception as error:
                print('Error:', error)
                views = 0
            posts = ''
            count = 0
            for post in page.find_all('div', class_='wall_reply_text'):
                count += 1
                posts += f'POST_{count}: {post.get_text()} | '
            return link, likes, reposts, views, posts
        except Exception as error:
            print('Error', error)
        return link, 0, 0, 0, ''
