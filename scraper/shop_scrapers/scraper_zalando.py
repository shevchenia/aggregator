import re
from scraper.scraper import Scraper
from crawler.models import Link
from aggregator.constants import SITE_NAMES
from aggregator.page_loader import PageLoader


class ZalandoScraper(Scraper):
    URL = SITE_NAMES.get('zalando')

    def scrape(self, link: Link) -> tuple:
        try:
            page = PageLoader().get_page_selenium(link.link)
            h1 = page.find('h1').get_text().split(' - ')
            title = h1[0]
            type = h1[1] if len(h1) == 2 else h1[0]
            price = float(re.findall(r'\d+,\d\d', page.find('div', class_="Bqz_1C").get_text())[0].replace(',', '.'))
            category = 'None'
            return link, title, price, type, category
        except Exception as error:
            print('Error', error)
            return link, 'None', 0, 'None', 'None'
