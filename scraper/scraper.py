from abc import ABC, abstractmethod
import concurrent.futures
from django.conf import settings
from crawler.models import Link


class Scraper(ABC):

    @abstractmethod
    def scrape(self, link: Link) -> tuple:
        pass

    def scrape_bulk(self, links: list):
        if settings.USE_MULTITHREAD:
            return self._scrape_multithreaded(links)
        else:
            return [self.scrape(link) for link in links]

    def _scrape_multithreaded(self, links: list) -> list:
        articles = []
        with concurrent.futures.ThreadPoolExecutor(max_workers=20) as executor:
            futures = [executor.submit(self.scrape, link=link) for link in links]
            for future in concurrent.futures.as_completed(futures):
                try:
                    articles.append(future.result())
                except:
                    print("Error")
        return articles
