from django.db import models
from crawler.models import Link


class Article(models.Model):
    link = models.OneToOneField(Link, on_delete=models.CASCADE)
    title = models.TextField()
    create_date = models.DateField()
    text = models.TextField()

    class Meta:
        ordering = ['id']

    def __str__(self):
        return self.title


class Product(models.Model):
    link = models.OneToOneField(Link, on_delete=models.CASCADE)
    title = models.TextField()
    price = models.DecimalField(max_digits=10, decimal_places=2)
    type = models.TextField()
    category = models.TextField()

    class Meta:
        ordering = ['id']

    def __str__(self):
        return self.title


class Video(models.Model):
    link = models.OneToOneField(Link, on_delete=models.CASCADE)
    name = models.TextField()
    views = models.IntegerField()
    likes = models.IntegerField()
    dislikes = models.IntegerField()
    data = models.DateField()

    class Meta:
        ordering = ['id']

    def __str__(self):
        return self.name


class Post(models.Model):
    link = models.OneToOneField(Link, on_delete=models.CASCADE)
    likes = models.IntegerField()
    reposts = models.IntegerField()
    views = models.IntegerField()
    posts = models.TextField()

    class Meta:
        ordering = ['id']

    def __str__(self):
        return self.link
