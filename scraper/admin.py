from django.contrib import admin
from scraper.models import Article, Product, Video, Post

admin.site.register(Article)
admin.site.register(Product)
admin.site.register(Video)
admin.site.register(Post)
